from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _ # Biên dịch label sang nhiều ngôn ngữ

# Register your models here.

# Lấy đường dẫn đến models User của mình tạo
User = get_user_model()


@admin.register(User)
class ProfilesUserAdmin(UserAdmin): #Kế thừa từ UserAdmin và sửa lại các trường cho phù hợp với models User
    """docstring for ProfilesUserAdmin"""
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("full_name", "phone_number", "email", "address")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2", 'email'),
            },
        ),
    )

    list_display = ("username", "full_name", "phone_number", "email", "address")
    list_filter = ("is_staff", "is_superuser", "is_active", "groups")
    search_fields = ("username", "full_name", "phone_number", "email")
    ordering = ("username",)
    filter_horizontal = (
        "groups",
        "user_permissions",
    )
