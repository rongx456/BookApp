from django import forms
from django.contrib.auth.forms import UsernameField, UserCreationForm # class creation account of django
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _


User = get_user_model() #Lấy đường dẫn đến models User

# Kế thừa từ lớp UserCreationForm để có các method làm sạch dữ liệu, đký...
class RegisterForm(UserCreationForm):
	email = forms.EmailField(required=True, 
			widget=forms.EmailInput(attrs={ 'class': "#"}))


	class Meta:
		model = User
		fields = ('username', 'email', 'password1', 'password2')
		field_classes = {'username': UsernameField}

		wiget = {
			# 'email': forms.EmailInput(attrs={'required':True, 'data-id': 1000})
		}


# Kế thừa thừ ModelForm để chọc thẳng vào Model
class ProfileForm(forms.ModelForm):

	class Meta:
		model = User
		fields = ('full_name', 'avatar', 'phone_number', 'email', 'address') #Các field có thể update