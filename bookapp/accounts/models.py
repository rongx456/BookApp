from django.db import models
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

# Create your models here.


# Custom Đăng ký tài khoản và thông tin tài khoản
class User(AbstractUser):
	"""docstring for User"""
	username_validator = UnicodeUsernameValidator()

	username = models.CharField(
		_("username"),
		max_length=150,
		unique=True,
		help_text=_(
		    "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
		),
		validators=[username_validator],
		error_messages={
		    "unique": _("A user with that username already exists."),
		},
	)
	first_name = None
	last_name = None

	avatar = models.ImageField(_("Avatar"), upload_to="avatars", null=True, blank=True)
	full_name = models.CharField(_("Full name"), max_length=100, blank=True)
	phone_number = models.CharField(_("Phone number"), max_length=10, blank=True)
	email = models.EmailField(_("Email address"), unique=True)
	address = models.CharField(_("Address"), max_length=400, blank=True)

	def __str__(self):
		return self.username
		