from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
# from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, FormView, UpdateView
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy

# import forms tự viết
from accounts.forms import *

#Lấy đường dẫn đến models User
User = get_user_model()

# Create your views here.


# Thêm LOGIN_REDIRECT_URL = "/" ở file settings để redirect về trang home
class SiteLoginView(LoginView):
	template_name = 'login/login.html'


# Tạo View đăng ký kế thừa từ FormView được django viết sẵn
class SiteRegisterView(FormView):
	"""docstring for SiteRegisterView"""
	template_name = 'register/register.html'
	form_class = RegisterForm

	# Override (ghi đè) phương thức xác thực form
	def form_valid(self, form):
		data = form.cleaned_data # Lấy dữ liệu được làm sạch từ form vào biến data
		new_user = User.objects.create_user(
			username = data['username'],
			password = data['password1'],
			email = data['email']
			)
		return redirect(reverse('login')) #Đăng ký thành công thì redirect về trang login


# Thêm LOGIN_REDIRECT_URL = "/" LOGOUT_REDIRECT_URL = "/" ở file settings để redirect về trang home
#Bug: ở urls path không có '/' thì không logout được. Fix thay "/" = "logout/" ở urls vì domain đã có 1 / rồi
class SiteLogoutView(LogoutView):
	template_name = 'ebook/home_ebook.html'


#Bug: Dùng hàm ở urls path không có '/' thì không logout được. Fix giống như class
# def logout_view(request):
#     logout(request)
#     # Redirect to a success page.
#     return redirect('home_ebook')

class EditProfileView(LoginRequiredMixin, UpdateView):
	"""docstring for EditProfileView"""
	template_name = 'profile/profile.html'
	# form_class = ProfileForm
	model = User
	fields = ('full_name', 'avatar', 'phone_number', 'email', 'address')
	success_url = reverse_lazy('profile')

	def get_object(self, queryset=None):
		return self.request.user

	# Hàm lấy dữ liệu rồi cho vào form (#Chưa biết làm cách này.)
	# def get_initial(self):
	# 	user = self.request.user
	# 	# from pprint import pprint; pprint(user.email)

	# 	return {
	# 		'Full name': user.full_name,
	# 		'Email address': user.email
	# 	}

	# def form_valid(self, form):
	# 	from pprint import pprint; pprint(form.cleaned_data)
	# 	data = form.cleaned_data
	# 	form.instance.id = self.request.user.id
	# 	form.save()

	# 	return redirect(self.success_url)