from django.urls import path
from . import views as acc_views

urlpatterns = [
	# path('register/', acc_views.register, name = "register"),
	path('login/', acc_views.SiteLoginView.as_view(), name = "login"),
	path('register/', acc_views.SiteRegisterView.as_view(), name = "register"),
	path('profile/', acc_views.EditProfileView.as_view(), name = "profile"),
	path('logout/', acc_views.SiteLogoutView.as_view(), name = "logout"),
]