from django.db import models
from django.contrib.auth import get_user_model
from django.utils.timezone import now as djnow
import uuid
from django.utils.translation import gettext as _

from django.db.models.signals import post_save
from django.dispatch import receiver


User = get_user_model()
# Create your models here.


class Customer(models.Model):
    class Meta:
        verbose_name = _("Khách hàng")
        verbose_name_plural = _("Khách hàng")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id Khách hàng"
                        )

    user = models.OneToOneField(User, 
                                on_delete=models.SET_NULL, 
                                related_name='%(class)s_user',
                                editable = False,
                                null=True, 
                                blank=False,
                                help_text='User name'
                                )

    name = models.CharField(max_length=100
                            )

    email = models.EmailField(null=True)

    phone_number = models.CharField(max_length=100, 
                                    null=True
                                    )

    created_by = models.ForeignKey(User,
                                   to_field='username',
                                   on_delete=models.SET_NULL,
                                   related_name='%(class)s_created_by',
                                   null=True,
                                   blank=True,
                                   help_text='Người tạo'
                                   )

    updated_by = models.ForeignKey(User,
                                   to_field='username',
                                   on_delete=models.SET_NULL,
                                   related_name='%(class)s_updated_by',
                                   null=True,
                                   blank=True,
                                   help_text='Người cập nhật'
                                   )

    updated_at = models.DateTimeField(
        default=djnow,
        help_text='Thời điểm cập nhật'
    )

    created_at = models.DateTimeField(
        default=djnow,
        editable=False,
        help_text='Ngày đăng tải')

    def __str__(self):
        return str(self.user)


class SCategory(models.Model):
    class Meta:
        verbose_name = _("Danh Mục")
        verbose_name_plural = _("Danh Mục")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id Danh Mục"
                        )

    name = models.CharField(max_length=200, unique=True)

    slug = models.SlugField()

    created_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_created_by',
                                   on_delete=(models.SET_NULL),
                                   null=True,
                                   blank=True,
                                   help_text='Người tạo'
                                   )

    updated_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_updated_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người cập nhật'
                                   )

    updated_at = models.DateTimeField(
        default=djnow,
        help_text='Thời điểm cập nhật'
    )

    created_at = models.DateTimeField(
        default=djnow,
        editable=False,
        help_text='Ngày đăng tải')

    def __str__(self):
        return self.name


class Product(models.Model):
    class Meta:
        verbose_name = _("Sản phẩm")
        verbose_name_plural = _("Sản phẩm")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id Danh Mục"
                        )


    scategory = models.ForeignKey(SCategory, 
                                on_delete=models.SET_NULL, 
                                blank=True, 
                                null=True
                                )

    name = models.CharField(max_length=200, 
                            null=True,
                            blank=True
                            )

    author = models.CharField(max_length=100, 
                                null=True,
                                blank=True, 
                                help_text="Tên tác giả"
                            ) # Tác giả

    publisher = models.CharField(max_length=100, 
                                null=True, 
                                blank=True,
                                help_text="Nhà xuất bản"
                                ) # Nhà xuất bản

    year = models.DateField(default=djnow, 
                                help_text="Năm xuất bản"
                                ) # Năm xuất bản

    stock = models.DecimalField(max_digits=10, 
                                null=True,
                                decimal_places=2,
                                help_text="Số lượng còn lại"
                                ) # Số lượng tồn kho

    description = models.TextField(max_length=1024,
                                    null=True,
                                    help_text="Mô tả nội dung"
                                    ) # Mô tả nội dung

    image = models.ImageField(upload_to='imagesproduct')

    price = models.FloatField(default=0)

    created_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_created_by',
                                   on_delete=(models.SET_NULL),
                                   null=True,
                                   blank=True,
                                   help_text='Người tạo'
                                   )

    updated_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_updated_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người cập nhật'
                                   )

    updated_at = models.DateTimeField(
        default=djnow,
        help_text='Thời điểm cập nhật'
    )

    created_at = models.DateTimeField(
        default=djnow,
        editable=False,
        help_text='Ngày đăng tải')
    
    def convert_money(self):
        try:
            return f"{self.price:,.2f}"
        except:
            return self.price

    def __str__(self):
        return str(self.name)


class Status(models.Model):
    class Meta:
        verbose_name = _("Trạng thái")
        verbose_name_plural = _("Những trạng thái")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id trạng thái"
                        )

    code = models.CharField(max_length=50, 
                            unique=True ,
                            help_text="Mã đơn hàng")

    name = models.CharField(max_length=100, 
                            help_text='Trạng thái đơn hàng'
                            )

    created_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_created_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người tạo'
                                   )

    updated_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_updated_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người cập nhật'
                                   )

    updated_at = models.DateTimeField(
        default=djnow,
        help_text='Thời điểm cập nhật'
    )

    created_at = models.DateTimeField(
        default=djnow,
        editable=False,
        help_text='Ngày đăng tải')

    def __str__(self):
        return self.name


class Order(models.Model):
    class Meta:
        verbose_name = _("Đơn hàng")
        verbose_name_plural = _("Đơn hàng")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id đơn hàng"
                        )
    
    user = models.ForeignKey(User,
                            on_delete=models.SET_NULL,
                            related_name='%(app_label)s_%(class)s_user',
                            null = True,
                            blank=True,
                            help_text="Người dùng",
                            )

    customer = models.ForeignKey(Customer, 
                                on_delete=models.SET_NULL, 
                                null=True, 
                                blank=True
                                )

    status = models.ForeignKey(Status, 
                                on_delete=models.SET_NULL,
                                null=True, 
                                blank=True,
                                help_text="Trạng thái đơn hàng"
                            )

    transaction_id = models.CharField(max_length=200, 
                                    null=True,
                                    blank=True
                                    )

    created_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_created_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người tạo'
                                   )

    updated_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_updated_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người cập nhật'
                                   )

    updated_at = models.DateTimeField(
        default=djnow,
        help_text='Thời điểm cập nhật'
    )

    created_at = models.DateTimeField(
        default=djnow,
        editable=False,
        help_text='Ngày đăng tải')

    def __str__(self):
        return str(self.customer)


class OrderItem(models.Model):
    class Meta:
        verbose_name = _("Item mua")
        verbose_name_plural = _("Item mua")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id item"
                        )

    product = models.ForeignKey(Product, 
                                on_delete=models.SET_NULL,
                                blank=True, 
                                null=True
                                )

    order = models.ForeignKey(Order, 
                            on_delete=models.SET_NULL,
                            blank=True, 
                            null=True
                            )

    quantity = models.IntegerField(default=1, 
                                    null=True, 
                                    blank=True
                                    )

    created_by = models.ForeignKey(User,
                               to_field='username',
                               related_name='%(class)s_created_by',
                               on_delete=models.SET_NULL,
                               null=True,
                               blank=True,
                               help_text='Người tạo'
                               )

    updated_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_updated_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người cập nhật'
                                   )

    updated_at = models.DateTimeField(
        default=djnow,
        help_text='Thời điểm cập nhật'
    )

    created_at = models.DateTimeField(
        default=djnow,
        editable=False,
        help_text='Ngày đăng tải')

    def __str__(self):
        return str(self.product)


class ShippingAddress(models.Model):
    class Meta:
        verbose_name = _("Địa chỉ")
        verbose_name_plural = _("Địa Chỉ")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id địa chỉ"
                        )

    customer = models.ForeignKey(Customer, 
                                on_delete=models.SET_NULL,
                                null=True,
                                blank=True
                                )

    order = models.ForeignKey(Order, 
                            on_delete=models.SET_NULL, 
                            null=True,
                            blank=True
                            )

    address = models.CharField(max_length=200, 
                                null=True
                                )

    city = models.CharField(max_length=200, 
                            null=True
                            )

    state = models.CharField(max_length=200, 
                            null=True
                            )

    phone = models.CharField(max_length=10, 
                            null=True,
                            blank=True,
                            )

    created_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_created_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người tạo'
                                   )

    updated_by = models.ForeignKey(User,
                                   to_field='username',
                                   related_name='%(class)s_updated_by',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   help_text='Người cập nhật'
                                   )

    updated_at = models.DateTimeField(
        default=djnow,
        help_text='Thời điểm cập nhật'
    )

    created_at = models.DateTimeField(
        default=djnow,
        editable=False,
        help_text='Ngày đăng tải')

    def __str__(self):
        return self.address


class Cart(models.Model):
    class Meta:
        verbose_name = _("Giỏ hàng")
        verbose_name_plural = _("Giỏ hàng")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id item"
                        )

    product = models.ForeignKey(Product, 
                               on_delete=models.CASCADE,
                               null = True,
                               blank=True,
                               help_text='Sản phẩm'
                               )
    user = models.ForeignKey(User, 
                                on_delete=models.SET_NULL, 
                                related_name='%(class)s_user',
                                null=True, 
                                blank=False,
                                help_text='User name'
                            )
    
    created_at = models.DateTimeField(
        default=djnow,
        editable=False,
        help_text='Ngày đăng tải')

    def __str__(self):
    	return str(self.user)


class Contact(models.Model):
    class Meta:
        verbose_name = _("Liên hệ")
        verbose_name_plural = _("Liên hệ")

    uuid = models.UUIDField(primary_key=True, 
                        default=uuid.uuid4,
                        editable=False,
                        help_text="Id liên hệ"
                        )

    name = models.CharField( 
                           max_length=100,
                           help_text='Họ tên'
                               )
    email = models.EmailField(help_text='email')

    phone_number = models.CharField(max_length=15,
                                   help_text='Số điện thoại'
                                   )

    message = models.TextField(
                                max_length=1024,
                                null=True,
                                help_text='Nội dung'
                                )

    created_at = models.DateTimeField(
                                    default=djnow,
                                    editable=False,
                                    help_text='Ngày đăng tải'
                                    )

    def __str__(self):
    	return self.name


# Tự động tạo ra bản ghi cutomer onetoone
# @receiver(post_save, sender=User)
# def created_customer(sender, instance, created, **kwargs):
#     if created:
#         Customer.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def created_cart(sender, instance, created, **kwargs):
#     if created:
#         Cart.objects.create(user=instance)

# post_save.connect(created_customer, sender=User)
# post_save.connect(created_cart, sender=User)