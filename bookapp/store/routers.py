from rest_framework import routers
from .rest_views import OrderItemViewSet
from .rest_views import OrderViewSet
from .rest_views import OrderForUserViewSet
from .rest_views import ContactViewSet
from .rest_views import ProductViewSet
from .rest_views import CartViewSet
from .rest_views import ShippingAddressViewSet
from .rest_views import CustomerViewSet
from .rest_views import StatusViewSet

router = routers.DefaultRouter()


router.register(r'OrderItem', OrderItemViewSet)
router.register(r'Order', OrderViewSet)
router.register(r'OrderForUser', OrderForUserViewSet)
router.register(r'contact', ContactViewSet)
router.register(r'Product', ProductViewSet)
router.register(r'cart', CartViewSet)
router.register(r'ShippingAddress', ShippingAddressViewSet)
router.register(r'Customer', CustomerViewSet)
router.register(r'Status', StatusViewSet)