from django.contrib import admin


from .models import Customer
from .models import SCategory
from .models import Product
from .models import Order
from .models import OrderItem
from .models import ShippingAddress
from .models import Cart
from .models import Contact
from .models import Status


# Register your models here.
class CustomerAdmin(admin.ModelAdmin):
	list_display = ('user', 'name', 'email', 'phone_number', 'created_by')
	search_fields = ('name',)
	list_filter = ('user',)
	# actions = ('delete_model', 'delete_queryset')


class SCategoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'slug', 'created_by')
	search_fields = ('name',)
	list_filter = ('created_by',)


class ProductAdmin(admin.ModelAdmin):
	list_display = ('name', 'author', 'publisher', 'year', 'scategory', 'description', 'stock', 'price', 'created_by')
	search_fields = ('name', 'author', 'publisher')
	list_filter = ('created_by', 'scategory', 'author', 'publisher')


class OrderAdmin(admin.ModelAdmin):
	list_display = ('customer', 'status', 'transaction_id', 'created_by')
	search_fields = ('customer',)
	list_filter = ('status', 'created_by')


class OrderItemAdmin(admin.ModelAdmin):
	list_display = ('product', 'order', 'quantity', 'created_by')
	search_fields = ('product',)
	list_filter = ('created_by',)


class ShippingAddressAdmin(admin.ModelAdmin):
	list_display = ('customer', 'order', 'address', 'phone', 'created_by')
	search_fields = ('customer',)
	list_filter = ('created_by',)


class CartAdmin(admin.ModelAdmin):
	list_display = ('uuid','product', 'user')
	search_fields = ('user',)
	# list_filter = ('customer',)


class StatusAdmin(admin.ModelAdmin):
	list_display = ('uuid','code', 'name')


class ContactAdmin(admin.ModelAdmin):
	list_display = ('name', 'email', 'phone_number', 'message', 'created_at')
	search_fields = ('name',)


admin.site.register(Customer, CustomerAdmin)
admin.site.register(SCategory, SCategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem, OrderItemAdmin)
admin.site.register(ShippingAddress, ShippingAddressAdmin)
admin.site.register(Cart, CartAdmin)
admin.site.register(Status, StatusAdmin)
admin.site.register(Contact, ContactAdmin)