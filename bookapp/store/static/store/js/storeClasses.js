function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Lấy CSRF Token
function getCSRFTokenValue() {
    return getCookie('csrftoken');
}

function getSessionIdValue() {
    return getCookie('sessionid');
}

function getSessionIdValue() {
    return getCookie('sessionid');
};

// Funtions convert money
function formatPrice(price) {
    let formatter = new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2 });
    let formattedNumber = formatter.format(price)
    return formattedNumber
}

function postContact() {
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(),
            // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });

    $(document).ready(function () {
        var formData = {
            "name": $("#contactname").val(),
            "email": $("#contactemail").val(),
            "phone_number": $("#contactphone").val(),
            "message": $("#contactMessage").val()
        };
        console.log(formData);
        $.ajax({
            url: CONTACT_API_URL,
            encode: true,
            type: "POST",
            timeout: 30000,
            data: formData,
            dataType: "json",
            success: function (data) {
                toastr.success('Gửi thành công');
                console.log(data);
            },
            error: function (data) {
                toastr.warning('Gửi thất bại');
                console.log(data);
            }
        });
    });
}


// Filter Store
function ProductFilter(uuid) {
    var slugFilter = "?scategory=";
    var tableId = "StoreProduct";
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(),
            // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });
    $.ajax({
        url: PRODUCT_API_URL + slugFilter + uuid,
        type: "GET",
        //async: false,
        cache: false,
        timeout: 30000,
        success: function (data) {
            console.log(data);
            $("#" + tableId).empty();
            var html = "";
            for (let index = 0; index < data.length; index++) {
                console.log(data[index])
                var price = formatPrice(data[index].price);
                html += `
				    <div class="card col-4 mb-5">
					    <div class="card-header">
						    `+ data[index].name + `
					    </div>
					    <div class="card-body">
						    <div class="d-inline">
							    <img src="`+ data[index].image + `" height="100px">
						    </div>
					    </div>
					    <div class="card-footer">
						    <div class="d-inline text-start mx-2">`+ price + ` VNĐ</div>
						    <div class="d-inline">
							    <a href="/store/detail/`+ data[index].uuid + `/" class="btn btn-outline-success">	
								<i class="far fa-eye"></i>
							    </a>
						    </div>
						    <div class="d-inline text-center">
							    <button id="add-to-cart" 
									class="btn btn-outline-info" 
									onclick="addCart('`+ data[index].uuid + `')">
								    <i class="fas fa-cart-plus"></i>
							    </button>
						    </div>
                            <div class="d-inline">
                                <button class="btn btn-outline-danger" 
                                onclick="OrderCreateModal('`+ data[index].uuid + `', '` + data[index].name + `', ` + data[index].price + `)">
                                <i class="fas fa-credit-card"></i>
                                </button>
                            </div>
					    </div>
                    </div>
                `
            }
            $("#" + tableId).append(html);
        },
        error: function (data) {
            console.log(data);
        }
    });
}


// Add Product to Cart
function addCart(uuid) {
    console.log(uuid)
    console.log(CART_API_URL)
    var formData = {
        "product": uuid,
    }
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(),
        },
        tryCount: 0,
        retryLimit: 3,
    });
    console.log(formData)
    $.ajax({
        url: CART_API_URL,
        encode: true,
        type: "POST",
        timeout: 30000,
        data: formData,
        dataType: "json",

        success: function (data) {
            console.log(data);
            toastr.success('Đã thêm vào giỏ hàng!');
        },
        error: function (data) {
            console.log(data);
            toastr.warning('Thêm vào giỏ hàng thất bại!');
        }
    });
}

// Get Cart for current user
function getCart() {
    var bodyTBId = "CartbodyTB";
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(),
            // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });
    $.ajax({
        url: CART_API_URL,
        type: "GET",
        cache: false,
        timeout: 30000,
        success: function (data) {
            console.log(data);
            $("#" + bodyTBId).empty();
            var html = "";
            for (let index = 0; index < data.length; index++) {
                var i = index + 1;
                var price = formatPrice(data[index].price_product);
                html += `
                    <tr>
                        <th class="text-center">`+ i + `</th>
                        <td class="text-center"><img src="`+ data[index].image_product + `" height="100px"></td>
                        <td class="text-center">`+ data[index].name_product + `</td>
                        <td class="text-center">`+ price + ` VNĐ</td>
                        <td class="text-center"><i class="far fa-trash-alt" onclick="EventDeleteApi('`+ data[index].uuid + `')"><i></i></td>
                    </tr>
                `
            }
            $("#" + bodyTBId).append(html);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

// Get Cart for current user
function getOrderForUser() {
    var formId = "OrderForUserId";
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(),
            // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });
    $.ajax({
        url: ORDER_FOR_USER_API_URL,
        type: "GET",
        cache: false,
        timeout: 30000,
        success: function (data) {
            console.log(data);
            console.log(ORDER_FOR_USER_API_URL);
            $("#" + formId).empty();
            var html = "";
            for (let index = 0; index < data.length; index++) {
                var i = index + 1;
                var price = formatPrice(data[index].orderitem_set[0].product.price);
                html += `
                    <div class="mb-1 col-xl-3">
                        <label class="small mb-1">Tên sản phẩm: </label>
                        <input class="small mb-1" value="`+ data[index].orderitem_set[0].product.name +`" readOnly>
                    </div>
                    <div class="mb-1 col-xl-3">
                        <label class="small mb-1">Số lượng: </label>
                        <input class="small mb-1" value="`+ data[index].orderitem_set[0].quantity +`" readOnly>
                    </div>
                    <div class="mb-1 col-xl-3">
                        <label class="small mb-1">Giá: </label>
                        <input class="small mb-1" value="`+ price +` VNĐ" readOnly>
                    </div>
                    <div class="mb-1 col-xl-3">
                        <label class="small mb-1">Trạng thái đơn hàng: </label>
                        <input class="small mb-1" value="`+ data[index].name_status +`" readOnly>
                    </div>
                `
            }
            $("#" + formId).append(html);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

// Run Cart
$(document).ready(function () {
    getCart()
    getOrderForUser()
})

// DELETE
function DeleteApi(uuid = null) {
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(),
        },
        tryCount: 0,
        retryLimit: 3,
    });
    var uuid_go = ""
    if (uuid == null) {
        uuid_go = cr_uuid;
    } else {
        uuid_go = uuid;
    }
    $.ajax({
        url: CART_API_URL + uuid_go + "/",
        type: "DELETE",
        async: false,
        cache: false,
        timeout: 30000,
        success: function (data) {
            toastr.success('Xóa thành công');
            getCart();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            if (xhr.textStatus == 'timeout') {
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    //try again
                    return;
                }
                return;
            }

            if (is_debug) {
                $.alert({
                    title: 'Error [' + xhr.status + '] ' + thrownError,
                    content: xhr.responseText,
                });
            }
        },
    });
}

// Delete API
function EventDeleteApi(uuid) {
    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'XÓA!',
        content: 'Bạn có chắc muốn xóa ?',
        theme: 'modern',
        closeIcon: 'cancel',
        animation: 'scale',
        type: 'orange',
        buttons: {
            cancel: {
                text: 'Hủy',
            },
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-blue',
                action: function () {
                    DeleteApi(uuid)
                }
            },

        }
    });
}

// Tạo đơn hàng
function OrderCreateModal(uuid, name, price) {
    $('#OrderCreatemodalsId').modal('toggle');
    fillFormModal(uuid, name, price, "Create")
    fillStatusModal("Create")
}

// Fill Form modal
function fillFormModal(uuid, name, price, modals_type) {
    var apart = modals_type + "Modal";

    try {
        var j_ele_uuid = $("#uuid" + apart + "InputId");
        if (j_ele_uuid.length > 0) {
            if (j_ele_uuid.attr('name') == 'product') {
                j_ele_uuid.val(uuid).change();
            }
        }
        else {
            // j_ele_uuid.val(null);
        }
    }
    catch (err) {
        console.log('err = ', err);
    };

    // Fill name lên modal
    try {
        var j_ele_name = $("#name_product" + apart + "InputId");
        if (j_ele_name.length > 0) {
            if (j_ele_name.attr('name') != 'uuid') {
                console.log(name)
                j_ele_name.val(name).change();
            }
        }
        else {
            // j_ele_name.val(null);
        }
    }
    catch (err) {
        console.log('err = ', err);
    };

    try {
        var j_ele_price = $("#price_product" + apart + "InputId");
        if (j_ele_price.length > 0) {
            if (j_ele_price.attr('name') != 'uuid') {
                j_ele_price.val(price).change();
            }
        }
        else {
            // j_ele_price.val(null);
        }
    }
    catch (err) {
        console.log('err = ', err);
    };
}

function fillStatusModal(modals_type) {
    var apart = modals_type + "Modal";
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(),
        },
        tryCount: 0,
        retryLimit: 3,
    });
    $.ajax({
        url: STATUS_API_URL,
        type: "GET",
        async: false,
        cache: false,
        timeout: 30000,
        success: function (data) {
            var html = '';
            try {
                var j_ele_stauts = $("#status" + apart + "InputId");
                j_ele_stauts.empty()
                if (j_ele_stauts.length > 0) {
                    if (j_ele_stauts.attr('name') != 'uuid') {
                        html += '<option value="' + data[0].uuid + '">' + data[0].name + '</option>'
                    }
                    $("#status" + apart + "InputId").append(html);
                }
                else {
                    // j_ele_status.val(null);
                }
            }
            catch (err) {
                console.log('err = ', err);
            };
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            if (xhr.textStatus == 'timeout') {
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    //try again
                    return;
                }
                return;
            }

            if (is_debug) {
                $.alert({
                    title: 'Error [' + xhr.status + '] ' + thrownError,
                    content: xhr.responseText,
                });
            }
        },
    });
}

// Create Address Customer
function CreateAddressApi(uuid, formId) {
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(), // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });
    //   var form = $('#' + formId);
    const form = document.getElementById(formId);
    var formData = new FormData(form);
    formData.set('order', uuid);
    $.ajax({
        url: ADDRESS_API_URL,
        type: "POST",
        async: false,
        cache: false,
        timeout: 30000,
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            console.log(data)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            if (xhr.textStatus == 'timeout') {
                return;
            }
        },
    });
};

// Create OrderItem
function CreateOrderItemApi(uuid, formId) {
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(), // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });
    //   var form = $('#' + formId);
    const form = document.getElementById(formId);
    var formData = new FormData(form);
    formData.set('order', uuid);
    $.ajax({
        url: ORDERITEM_API_URL,
        type: "POST",
        async: false,
        cache: false,
        timeout: 30000,
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            console.log(data)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
        },
    });
};

// Create Order
function CreateOrderApi(uuid, formId) {
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(), // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });
    const form = document.getElementById(formId);
    var formData = new FormData(form);
    formData.set('customer', uuid);
    $.ajax({
        url: ORDER_API_URL,
        type: "POST",
        async: false,
        cache: false,
        timeout: 30000,
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            console.log(data)
            try {
                CreateAddressApi(data.uuid, "AddressCreateModalsFormId")
                CreateOrderItemApi(data.uuid, "ProductCreateModalsFormId")
                toastr.success("Tạo đơn hàng thành công")
                $('.modal').modal('hide');
            } catch (error) {
                toastr.warning("Tạo đơn hàng thất bại!")
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
        },
    });
};

// Create Customer
function CreateCustomerApi(formId) {
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(), // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });
    //   var form = $('#' + formId);
    const form = document.getElementById(formId);
    var formData = new FormData(form);
    $.ajax({
        url: CUSTOMER_API_URL,
        type: "POST",
        timeout: 30000,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            console.log("uuid = ",data.uuid)
            CreateOrderApi(data.uuid, "OrderCreateModalsFormId")
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            if (xhr.textStatus == 'timeout') {
                return;
            }
        },
    });
};

// [Save Create] Clicked Handle function
$(document).ready(function () {
    $("#OrderCreateModalBtnId").click(function () {
        var validate_obj_1 = new InputValidation('ProductCreateModalsFormId');
        var validate_obj_2 = new InputValidation('CustomerCreateModalsFormId');
        var validate_obj_3 = new InputValidation('AddressCreateModalsFormId');
        if (validate_obj_1.validateRequired()
            || validate_obj_2.validateRequired()
            || validate_obj_3.validateRequired()) {
            toastr.warning('Vui lòng điền đầy đủ thông tin');
            return;
        }
        CreateCustomerApi("CustomerCreateModalsFormId")
    })
});


// [GET] Đơn hàng của User


// Test
class InputValidation {
    Elements = []
    Elements_number = []
    Elements_mail = []
    constructor(FormId) {
        this.formId = FormId;
        var required_len = $(".dvd-required").length;
        var elements = []
        if (required_len > 0) {
            $("#" + FormId).find(".dvd-required").each(function () {

                elements.push($(this));

            })
            this.Elements = elements;
        }
        var number_len = $("[type=number]").length;
        var elements = []
        if (number_len > 0) {
            $("#" + FormId).find("[type=number]").each(function () {

                elements.push($(this));

            })
            this.Elements_number = elements;
        }
        var mail_len = $("[mpstype= mail]").length;
        var elements = []
        if (mail_len > 0) {
            $("#" + FormId).find("[mpstype= mail]").each(function () {

                elements.push($(this));

            })
            this.Elements_mail = elements;
        }
    }
    removeAllWhiteSpace() {
        var is_success = true;
        try {
            $("#" + this.formId).find("input").each(function () {
                if ($(this).val() != undefined || $(this).val() != 'undefined') {
                    $(this).val($(this).val().trim())
                }
            })

        }
        catch (err) {
            is_success = false
        }
        return is_success
    }
    validateRequired() {
        if (!this.removeAllWhiteSpace()) {
            // toastr.warning("Xóa khoảng trắng lỗi!");
            // return false;
        }
        var is_has_invalid = false;
        is_has_invalid = this.validateRangeDate();
        if (this.Elements.length > 0) {
            this.Elements.forEach(element => {
                var title = element.parent("div").find("label").html();
                var div_parent = element.parent("div");
                div_parent.find(".invalid-feedback").remove();
                div_parent.find(".valid-feedback").remove();
                if (element.val() == "" || element.val() == null || element.val() == undefined) {
                    if (element.is(":visible")) {
                        element.addClass("is-invalid").removeClass("is-valid");
                        // var invalid_txt = `
                        // <div class="invalid-feedback">
                        // <b>${title}</b> yêu cầu điền đầy đủ
                        // </div>`;

                        // div_parent.append(invalid_txt);
                        is_has_invalid = true;
                    }


                } else {
                    element.removeClass("is-invalid").addClass("is-valid");
                    // var valid_txt = `
                    // <div class="valid-feedback">
                    // <b>${title}</b> ok!
                    // </div>`;

                    // div_parent.append(valid_txt);
                }
            });
        }

        return is_has_invalid;
    }
    validateNumber() {
        var is_has_invalid = false;
        if (this.Elements_number.length > 0) {
            this.Elements_number.forEach(element => {
                if ($(element).val() != ""
                    && ($(element).val().includes("-")
                        || $(element).val().includes(".")
                        || $(element).val().includes(",")
                        || parseInt($(element).val()) != $(element).val())) {
                    element.addClass("is-invalid").removeClass("is-valid");
                    is_has_invalid = true;
                }
            })
        }

        return is_has_invalid;
    }
    validateMail() {
        var is_has_invalid = false;
        var regExp = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
        if (this.Elements_mail.length > 0) {
            this.Elements_mail.forEach(element => {
                if (!$(element).val().match(regExp)) {
                    element.addClass("is-invalid").removeClass("is-valid");
                    is_has_invalid = true;
                }
            })
        }

        return is_has_invalid;
    }
    validateRangeDate() {
        var is_has_date_invalid = false;
        var start_date = $("#" + this.formId).find("[name=start_date]");
        var end_date = $("#" + this.formId).find("[name=end_date]");
        var expriry_date = $("#" + this.formId).find("[name=expriry_date]");
        if (start_date.length > 0) {
            var momentA = moment(start_date.val(), "DD/MM/YYYY");

            if (end_date.length > 0) {

                var momentB = moment(end_date.val(), "DD/MM/YYYY");
                if (momentA > momentB) {
                    toastr.error('Thời gian bắt đầu lớn hơn thời gian kết thúc');
                    is_has_date_invalid = true;
                }
            }
            if (expriry_date.length > 0) {

                var momentB = moment(expriry_date.val(), "DD/MM/YYYY");
                if (momentA > momentB) {
                    toastr.error('Thời gian bắt đầu lớn hơn thời gian kết thúc');
                    is_has_date_invalid = true;
                }
            }
        }
        return is_has_date_invalid;

    }
    validateRequiredNotice() {
        var is_has_invalid = false;
        if (this.Elements.length > 0) {

            this.Elements.forEach(element => {
                var title = element.parent("div").find("label").html();
                var div_parent = element.parent("div");
                div_parent.find(".invalid-feedback").remove();
                div_parent.find(".valid-feedback").remove();
                element.addClass("is-invalid").removeClass("is-valid");
                var invalid_txt = `
                    <div class="invalid-feedback">
                    Vui lòng điền <b>${title}</b>
                    </div>`;
                div_parent.append(invalid_txt);
            });
        }
        return is_has_invalid;
    }


}