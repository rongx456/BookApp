from rest_framework import serializers
from django.contrib.auth.models import AnonymousUser

from .models import OrderItem
from .models import Order
from .models import Contact
from .models import Product
from .models import Cart
from .models import ShippingAddress
from .models import Customer
from .models import Status


# Serializers define the API representation.

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
                    "uuid",
                    "name",
                    "scategory",
                    "scategory",
                    "author",
                    "publisher",
                    "year",
                    "stock",
                    "description",
                    "image",
                    "price",
                    "created_by",
                    "created_at",
                )


class OrderItemSerializer(serializers.ModelSerializer):
    # product = ProductSerializer()
    class Meta:
        model = OrderItem
        fields = (
                    'uuid', 
                    'product', 
                    'order', 
                    'quantity',
                )


# Cho xem dơn hàng 
class OrderItemForOrderSerializer(serializers.ModelSerializer):
    product = ProductSerializer()
    class Meta:
        model = OrderItem
        fields = (
                    'uuid', 
                    'product', 
                    'order', 
                    'quantity',
                )


class ShippingAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShippingAddress
        fields = (
                    'uuid', 
                    'customer', 
                    'order', 
                    'phone', 
                    'city',
                    'state',
                    'address',
                )


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = (
                    'uuid', 
                    'user', 
                    'name', 
                    'email', 
                    'phone_number',
                )


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = (
                    'uuid', 
                    'name', 
                    'email', 
                    'phone_number',
                    'message',
                    'created_at',
                )


class ProductShortSerializer(serializers.ModelSerializer):
    uuid_scategory = serializers.CharField(source='scategory.uuid',read_only=True)
    class Meta:
        model = Product
        fields = ['uuid', 'uuid_scategory','scategory','name', 'price']


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = (
                "uuid",
                "user",
                "customer",
                "status",
            )
    
    def get_request_user(self):
        request_user = None
        if 'request' in self.context and hasattr(self.context['request'], "user"):
            request_user = self.context['request'].user
            if isinstance(request_user, AnonymousUser):
                request_user = None
        return request_user

    def create(self, validated_data):
        validated_data['user'] = self.get_request_user()
        return super().create(validated_data)


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = (
                    'uuid', 
                    'name', 
                )


class CartSerializer(serializers.ModelSerializer):
    name_product = serializers.CharField(source='product.name',read_only=True)
    price_product = serializers.CharField(source='product.price',read_only=True)
    image_product = serializers.CharField(source='product.image.url',read_only=True)
    name_user = serializers.CharField(source='user.username',read_only=True)
    class Meta:
        model = Cart
        fields = ['uuid', 'image_product', 'name_product', 'price_product', 'product', 'name_user', 'user', 'created_at']
    
    def get_request_user(self):
        request_user = None
        if 'request' in self.context and hasattr(self.context['request'], "user"):
            request_user = self.context['request'].user
        return request_user

    def create(self, validated_data):
        validated_data['user'] = self.get_request_user()
        return super().create(validated_data)

    # def update(self, instance, validated_data):
    #     validated_data['updated_by'] = self.get_request_user()
    #     return super().update(instance, validated_data)
    

class OrderForUserSerializer(serializers.ModelSerializer):
    orderitem_set = OrderItemForOrderSerializer(many=True)
    name_status = serializers.CharField(source='status.name',read_only=True)
    
    class Meta:
        model = Order
        fields = (
            (
                "uuid",
                "orderitem_set",
                "user",
                "customer",
                "name_status",
                "status",
            )
        )