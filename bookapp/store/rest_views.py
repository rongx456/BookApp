from rest_framework import viewsets
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import AllowAny


import operator
from django.db.models import Q

from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
            
from rest_framework import filters
# from Router.models import *
import django_filters.rest_framework

from .models import OrderItem
from .models import Order
from .models import Contact
from .models import Product
from .models import Cart
from .models import ShippingAddress
from .models import Customer
from .models import Status

from .serializers import OrderItemSerializer
from .serializers import OrderSerializer
from .serializers import ContactSerializer
from .serializers import ProductSerializer
from .serializers import CartSerializer
from .serializers import ShippingAddressSerializer
from .serializers import CustomerSerializer
from .serializers import StatusSerializer
from .serializers import OrderForUserSerializer


# Short API
from .serializers import ProductShortSerializer


class OrderItemViewSet(viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    permission_classes = [AllowAny]


class StatusViewSet(viewsets.ModelViewSet):
    queryset = Status.objects.all()
    serializer_class = StatusSerializer
    permission_classes = [AllowAny]
    def get_queryset(self):
        return Status.objects.filter(uuid=Status.objects.last().uuid)


class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    permission_classes = [AllowAny]
    http_method_names = ["get", "post", "put", "patch", "delete"]


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [AllowAny]


class OrderForUserViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderForUserSerializer
    permission_classes = [IsAuthenticated]
    http_method_names = ["get",]
    # Ghi đè hàm để có thể dùng filter, orderby,...
    def get_queryset(self):
        queryset = self.queryset
        crr_user = self.request.user
        query_set_by_request_user = queryset.filter(user=crr_user).all()
        return query_set_by_request_user


class ShippingAddressViewSet(viewsets.ModelViewSet):
    queryset = ShippingAddress.objects.all()
    serializer_class = ShippingAddressSerializer
    permission_classes = [AllowAny]


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [AllowAny]



# API Product
class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [AllowAny]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = {'scategory': ['exact']}


# API Cart
class CartViewSet(viewsets.ModelViewSet):
    queryset = Cart.objects.all().order_by('-created_at')
    serializer_class = CartSerializer
    permission_classes = [IsAuthenticated]
    
    def get_queryset(self):
        queryset = self.queryset
        crr_user = self.request.user
        query_set_by_request_user = queryset.filter(user=crr_user).all()
        return query_set_by_request_user