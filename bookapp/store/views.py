from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from django.views.generic import DetailView
from django.db.models import Q
from django.template import loader

	
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger

from django.shortcuts import get_object_or_404

from .models import SCategory, Product, Cart

# Create your views here.
def home_store(request):
	# Search
	query = request.GET.get("q")
	if query:
		products = Product.objects.filter(
				Q(name__icontains=query)
				# | Q(scategory__icontains=query)
			)
	else:
		products = Product.objects.order_by('-updated_at').all()

	# Phân trang
	paginator = Paginator(products, 1, request=request) # Khởi tạo Paginator
	try:
		pageNumber = request.GET.get('page', 1)
	except PageNotAnInteger:
		pageNumber = 1
	except EmptyPage:
		pageNumber = 1
	products = paginator.page(pageNumber) # Lấy đối tượng Page

	categorys = SCategory.objects.all()
	context = {'nav': 'home_store', 'categorys': categorys, 'products':products}
	return render(request, 'store/home_store.html', context) # Truyền Page vào mẫu html

# def category(request):
# 	categorys = SCategory.objects.all()
# 	context = {'categorys': categorys}
# 	return render(request, 'store/category_filter.html', context)

@login_required(login_url="/accounts/login/")
def cart(request, *args, **kwargs):
	context = {}

	template = loader.get_template(str('store/cart.html'))

	all_of_objs = Cart.objects.all().order_by('-created_at')

	context['cart'] = all_of_objs

	result = HttpResponse(template.render(context, request))
	return result

@login_required(login_url="/accounts/login/")
def order(request, *args, **kwargs):
	context = {}

	template = loader.get_template(str('store/order.html'))

	all_of_objs = Cart.objects.all().order_by('-created_at')

	context['order'] = all_of_objs

	result = HttpResponse(template.render(context, request))
	return result


class ProductDetailView(DetailView):
	"""docstring for DetailView"""
	model = Product
	template_name = 'store/details.html'


def contact(request):
	context = {'nav':'contact'}
	return render(request, 'store/contact.html', context)
