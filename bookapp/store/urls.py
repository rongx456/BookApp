from django.urls import path, include
from . import views
from .routers import router
from .rest_views import ProductViewSet

urlpatterns = [
    path('', views.home_store, name='home_store'),
    path('contact/', views.contact, name = "contact"),
    # path('category/', views.category, name='category'),
    path('cart/', views.cart, name='cart'),
    path('order/', views.order, name='order'),
    path('detail/<uuid:pk>/', views.ProductDetailView.as_view(), name='detail'),
    # path('store/search/', views.SearchView.as_view(), name='search_results'),

    #API Book Store
    path('api/', include(router.urls)),
]