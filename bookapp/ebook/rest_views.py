from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated

from .serializers import FeedbackSerializer
from .models import Feedback


class FeedbackViewSet(viewsets.ModelViewSet):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    permission_classes = [IsAuthenticated]
    
    def get_queryset(self):
        # return super().get_queryset()
        queryset = self.queryset
        messages_active = queryset.filter(active=True).all()
        return messages_active