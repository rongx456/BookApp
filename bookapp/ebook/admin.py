from django.contrib import admin
from ebook.models import ECategory, EBook, Feedback, Contribute


# Register your models here.
class EBookAdmin(admin.ModelAdmin):
	list_display = ('ebook_title', 'author', 'uploaded_by')
	search_fields = ('ebook_title',)
	list_filter = ('author', 'uploaded_by')


class ECategoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'slug')
	search_fields = ('name',)


class FeedbackAdmin(admin.ModelAdmin):
	list_display = ('user_id', 'message', 'time_send', 'active')
	search_fields = ('user_id',)
	list_filter = ('active',)


class ContributeAdmin(admin.ModelAdmin):
	list_display = ('user_id', 'ebook_cover', 'ebook_title', 'pdf', 'author', 'source')
	search_fields = ('ebook_title',)
	list_filter = ('category', 'user_id')


admin.site.register(ECategory, ECategoryAdmin)
admin.site.register(EBook, EBookAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Contribute, ContributeAdmin)
