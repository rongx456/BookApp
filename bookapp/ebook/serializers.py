from rest_framework import serializers

from .models import Feedback


# Serializers define the API representation.
class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = [
            'user_id', 
            'message', 
            'time_send', 
            'active',
        ]

    def get_request_user(self):
        request_user = None
        if 'request' in self.context and hasattr(self.context['request'], "user"):
            request_user = self.context['request'].user
        return request_user
    
    def create(self, validted_data):
        validted_data["user_id"] = self.get_request_user()
        return super().create(validted_data)