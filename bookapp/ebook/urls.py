from django.urls import path, include

from .routers import router
from . import views as ebook_views

urlpatterns = [
    path('', ebook_views.home_ebook, name = "home_ebook"),
    path('about/', ebook_views.about, name = "about"),
    path('contribute/', ebook_views.ContributeView.as_view(), name='contribute'),
    path('messages/', ebook_views.messages, name = "messages"),

    #API
    path('ebook/api/', include(router.urls)),
]