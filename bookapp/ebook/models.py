from django.db import models
from django.contrib.auth import get_user_model
from django.utils.timezone import now as djnow


# Create your models here.

User = get_user_model()

class ECategory(models.Model):
	name = models.CharField(max_length=200,  
							null=True, 
							blank=True, 
							help_text='Tên loại sách'
							)

	slug = models.SlugField(
							help_text='Slug'
							)

	def __str__(self):
		return self.name


class EBook(models.Model):
	"""docstring for EBook"""
	ebook_cover = models.ImageField(upload_to = 'ebookcover', help_text='Ảnh bìa sách')

	ebook_title = models.CharField(max_length=100, help_text='Tên sách')

	pdf = models.FileField(upload_to = 'pdfs', help_text='File sách')

	author = models.CharField(max_length=150, 
							null=True, 
							blank=False,
							help_text='Tác giả'
							)

	category = models.ForeignKey(ECategory, 
								on_delete=models.SET_NULL, 
								null=True, 
								blank=True,
								help_text='Loại sách'
								)

	uploaded_by = models.ForeignKey(User, 
									to_field='username', 
									on_delete=models.CASCADE, 
									null=True, 
									blank=True,
									help_text='Người đăng tải'
									)

	created_at = models.DateTimeField(default=djnow, 
									editable=False,
									help_text='Ngày đăng tải'
									)

	sell = models.BooleanField(default=False, help_text='CÓ bán hay không?')

	price = models.IntegerField(default=0, help_text='Giá bán')

	def __str__(self):
		return self.ebook_title


class Feedback(models.Model):
	user_id = models.ForeignKey(User, 
								to_field='username', 
								on_delete=models.CASCADE, 
								null=True, 
								blank=True,
								help_text='Người viết'
								)

	message = models.TextField(max_length=1024)

	time_send = models.DateTimeField(default=djnow,
									editable=False,
									help_text='Thời gian đóng góp'
									)

	active = models.BooleanField(default=True)

	def __str__(self):
		return str(self.user_id)
						

class Contribute(models.Model):
	"""docstring for Contribute"""
		
	user_id = models.ForeignKey(User, 
								to_field='username', 
								on_delete=models.CASCADE, 
								null=True, 
								blank=True,
								help_text='Người đóng góp'
								)

	ebook_cover = models.ImageField(upload_to = 'contribute/ebookcover', 
									help_text='Ảnh bìa sách'
									)

	ebook_title = models.CharField(max_length=150, help_text='Tên sách')

	pdf = models.FileField(upload_to = 'contribute/pdfs', 
							help_text='File sách'
							)

	author = models.CharField(max_length=100, help_text='Tác giả')

	category = models.ForeignKey(ECategory,
								on_delete=models.SET_NULL, 
								null=True, 
								blank=True,
								help_text='Loại sách'
								)

	source = models.CharField(max_length = 100, blank=True, help_text='Nguồn sách')

	current_update = models.DateTimeField(default=djnow,
										editable=False,
										help_text='Thời gian đóng góp',
										)

	def __str__(self):
		return str(self.user_id)