from django import forms
from ebook.models import Feedback, Contribute
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _


# Form FeedbackForm
class FeedbackForm(forms.ModelForm):
	"""docstring for FeedbackForm"""
	class Meta:
		model = Feedback
		fields = ('message',)


# Form Contribute
class ContributeForm(forms.ModelForm):
	"""docstring for Contribute"""
	class Meta:
		model = Contribute
		fields = ('ebook_cover', 'ebook_title', 'pdf', 'author', 'category', 'source')