from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, FormView
from ebook.forms import FeedbackForm, ContributeForm
from django.views.generic import CreateView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView
from django.db.models import Q

from pure_pagination import Paginator, EmptyPage, PageNotAnInteger


from ebook.models import Feedback, Contribute, EBook

# Create your views here.
def home_ebook(request):
	# ebooks = EBook.objects.order_by('-created_at').all() # Lấy all dữ liệu từ bảng EBook
	# Search
	query = request.GET.get("q")
	if query:
		ebooks = EBook.objects.filter(
				Q(ebook_title__icontains=query)
				| Q(author__icontains=query)
			)
	else:
		ebooks = EBook.objects.order_by('-created_at').all()

	# Phân trang
	paginator = Paginator(ebooks, 2, request=request) # Khởi tạo Paginator
	try:
		pageNumber = request.GET.get('page', 1)
	except PageNotAnInteger:
		pageNumber = 1
	except EmptyPage:
		pageNumber = 1
	ebooks = paginator.page(pageNumber) # Lấy đối tượng Page

	context = {'ebooks': ebooks, 'nav':'home_ebook'}
	return render(request, 'ebook/home_ebook.html', context)

def about(request):
	context = {'nav':'about'}
	return render(request, 'ebook/about.html', context)


class ContributeView(LoginRequiredMixin, CreateView):
	form_class = ContributeForm
	model = Contribute
	template_name = 'ebook/contribute.html'
	success_url = reverse_lazy('home_ebook')

	def form_valid(self, form):
		self.object = form.save(commit=False)
		self.object.user_id = self.request.user
		self.object.ebook_cover = self.request.POST['ebook_cover']
		self.object.ebook_cover = self.request.POST['pdf']
		self.object.save()
		return super().form_valid(form)


@login_required
def messages(request):
	messages = Feedback.objects.filter(active=True).all()
	context = {'messages': messages}
	return render(request, 'ebook/community_messages.html', context)
