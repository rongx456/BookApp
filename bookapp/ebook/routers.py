from rest_framework import routers
from .rest_views import FeedbackViewSet


router = routers.DefaultRouter()

router.register(r'feedback', FeedbackViewSet)