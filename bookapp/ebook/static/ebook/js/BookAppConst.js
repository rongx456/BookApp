SERVER = "";

const FEEDBACK_API_URL = SERVER + "/ebook/api/feedback/";

const CONTACT_API_URL = SERVER + "/store/api/contact/";

const PRODUCT_API_URL = SERVER + "/store/api/Product/";

const CART_API_URL = SERVER + "/store/api/cart/";

const STATUS_API_URL = SERVER + "/store/api/Status/";

const ORDER_API_URL = SERVER + "/store/api/Order/";

const ORDER_FOR_USER_API_URL = SERVER + "/store/api/OrderForUser/";

const ADDRESS_API_URL = SERVER + "/store/api/ShippingAddress/";

const ORDERITEM_API_URL = SERVER + "/store/api/OrderItem/";

const CUSTOMER_API_URL = SERVER + "/store/api/Customer/";