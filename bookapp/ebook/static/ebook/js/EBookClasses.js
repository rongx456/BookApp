function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Lấy CSRF Token
function getCSRFTokenValue() {
    return getCookie('csrftoken');
}

function getSessionIdValue() {
    return getCookie('sessionid');
}

function clickedit() {
    $(document).ready(function() {
        let check = "disabled";
        console.log(check);
        return check;
    });
};

function getSessionIdValue() {
    return getCookie('sessionid');
};

function postFeedback() {
    $.ajaxSetup({
        headers: {
            'CSRFToken': getCSRFTokenValue(),
            'X-CSRFToken': getCSRFTokenValue(),
            // for --> SessionAuthentication
        },
        tryCount: 0,
        retryLimit: 3,
    });
    var self = this;

    $(document).ready(function() {
        var formData = {
            "message": $("#message").val(),
            "active": true
        };
        console.log(formData);
        $.ajax({
            url: FEEDBACK_API_URL,
            encode: true,
            type: "POST",
            timeout: 30000,
            data: formData,

            dataType: "json",
            success: function(data) {
                console.log(data);
            },
            error: function(data) {
                alert('Thất bại!');
                console.log(data);
            }
        });
    });
}