# Generated by Django 3.2.10 on 2023-05-02 13:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ECategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, help_text='Tên loại sách', max_length=200, null=True)),
                ('slug', models.SlugField(help_text='Slug')),
            ],
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message', models.TextField(max_length=1024)),
                ('time_send', models.DateTimeField(default=django.utils.timezone.now, editable=False, help_text='Thời gian đóng góp')),
                ('active', models.BooleanField(default=True)),
                ('user_id', models.ForeignKey(blank=True, help_text='Người viết', null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, to_field='username')),
            ],
        ),
        migrations.CreateModel(
            name='EBook',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ebook_cover', models.ImageField(help_text='Ảnh bìa sách', upload_to='ebookcover')),
                ('ebook_title', models.CharField(help_text='Tên sách', max_length=100)),
                ('pdf', models.FileField(help_text='File sách', upload_to='pdfs')),
                ('author', models.CharField(help_text='Tác giả', max_length=150, null=True)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False, help_text='Ngày đăng tải')),
                ('sell', models.BooleanField(default=False, help_text='CÓ bán hay không?')),
                ('price', models.IntegerField(default=0, help_text='Giá bán')),
                ('category', models.ForeignKey(blank=True, help_text='Loại sách', null=True, on_delete=django.db.models.deletion.SET_NULL, to='ebook.ecategory')),
                ('uploaded_by', models.ForeignKey(blank=True, help_text='Người đăng tải', null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, to_field='username')),
            ],
        ),
        migrations.CreateModel(
            name='Contribute',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ebook_cover', models.ImageField(help_text='Ảnh bìa sách', upload_to='contribute/ebookcover')),
                ('ebook_title', models.CharField(help_text='Tên sách', max_length=150)),
                ('pdf', models.FileField(help_text='File sách', upload_to='contribute/pdfs')),
                ('author', models.CharField(help_text='Tác giả', max_length=100)),
                ('source', models.CharField(blank=True, help_text='Nguồn sách', max_length=100)),
                ('current_update', models.DateTimeField(default=django.utils.timezone.now, editable=False, help_text='Thời gian đóng góp')),
                ('category', models.ForeignKey(blank=True, help_text='Loại sách', null=True, on_delete=django.db.models.deletion.SET_NULL, to='ebook.ecategory')),
                ('user_id', models.ForeignKey(blank=True, help_text='Người đóng góp', null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, to_field='username')),
            ],
        ),
    ]
